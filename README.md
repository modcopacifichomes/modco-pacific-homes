We are a full service modular builder, and can handle your project from beginning to end. While your building is being constructed in our factory, our team will take care of permitting, foundation work, delivery, installation, and service connections – getting you into your new home faster.

Address: 38902 Bowen Ave, Squamish, BC V8B 0E8, Canada

Phone: 604-332-1233

Website: https://www.modcopacific.ca
